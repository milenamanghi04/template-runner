using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerVida : MonoBehaviour
{
    private Slider slider;
    public float tiempo;
    public float agua;
    public float tiempoMax;
    public static bool AgarraAgua;
    public GameManager gameManager;

    void Start()
    {

        tiempo = tiempoMax;
        slider = GetComponent<Slider>();
        
    }
    void Update()
    {
        ControlTiempo();
        if(tiempo <= 0)
        {
            GameManager.gameOver = true;
        }
        if(AgarraAgua == true)
        {
            TomarAgua();
            AgarraAgua = false;
        }

        if(tiempo > tiempoMax)
        {
            tiempo = tiempoMax;
        }
    }

    public void CambiarVidaMaxima(float tiempo)
    {
        slider.maxValue = tiempoMax;
    }

    public void CambiarVidaActual(float tiempo)
    {
        slider.value = tiempo;
    }

    public void InicializarBarraDeVida(float tiempo)
    {
        CambiarVidaMaxima(tiempo);
        CambiarVidaActual(tiempo);
    }

    public void ControlTiempo()
    {
        tiempo -= Time.deltaTime;
       InicializarBarraDeVida(tiempo);
    }

    public void TomarAgua()
    {
       
         tiempo += agua;
        
       
    }


  
}
