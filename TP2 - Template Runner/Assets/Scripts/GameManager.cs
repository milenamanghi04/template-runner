using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public ControllerFileRecord datosRecord;  
    public Text recordText;
    public static bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    public float distance = 0;
    public int maxRecord;
    public bool gameStart = false;
    public GameObject player;
    public GameObject instantiator;
    public GameObject parallax;
    public int contInicioInstantiator = 0;

    // Inicializacion del juego
    void Start()
    {
        //maxRecord y recordText corresponden a FileRecord
        maxRecord = datosRecord.LoadRecord();
        recordText.text =  maxRecord.ToString();
        //gameOver, distance y gameOver corresponden al control de canvas
        gameOver = false;
        distance = 0;
        distanceText.text = distance.ToString();
        gameOverText.gameObject.SetActive(false);
    }
 
    // Cuerpo principal del juego
    void Update()
    {
        
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("0");
            gameOverText.gameObject.SetActive(true);
            Parallax.Detener = true;
        }
        if(gameStart)
        {
            distance += Time.deltaTime;
            distanceText.text = distance.ToString("0");
            datosRecord.SaveRecord((int)distance, maxRecord);
            //cuando en control canvas se hace click en btninicio se activa el juego por lo tanto se activa el player, instantiator, y parallax
            if(player.gameObject != null)
            {
                player.gameObject.SetActive(true);
                instantiator.gameObject.SetActive(true);
            }
            parallax.gameObject.SetActive(true);
            
        }
        //if(Input.GetKeyDown("R")){ }
    }
    
}
