using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Canvas : MonoBehaviour
{
  
    public GameObject CanvasInicio;
    public GameObject CanvasRecord;
    public GameObject CanvasAyuda;
    public GameManager gameManager;

    public void btnClick()
    {
       Debug.Log("BUTTON PRESSED: "+ this.name);
        if (this.name == "btnInicio")
        {
            CanvasInicio.SetActive(false);
            CanvasAyuda.SetActive(false);
            CanvasRecord.SetActive(true);
            gameManager.gameStart = true;

        }
        else if (this.name == "btnAyuda")
        {
            CanvasInicio.SetActive(false);
            CanvasAyuda.SetActive(true);
            CanvasRecord.SetActive(false);
        }
        else if (this.name == "btnCerrarAyuda")
        {
            CanvasInicio.SetActive(true);
            CanvasAyuda.SetActive(false);
            CanvasRecord.SetActive(false);
        }

    }
}

