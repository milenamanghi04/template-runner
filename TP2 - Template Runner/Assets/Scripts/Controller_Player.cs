﻿using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    private Rigidbody rb;
    public float jumpForce = 10;
    public float initialSize;
    private int i = 0;
    private bool floored;
    public float flyForce;
    public GameManager gameManager;


    private void Start()
    {

        

        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        

      
    }

    void Update()
    {
        GetInput();
        
    }

    public void GetInput()
    {
        Jump();
        Duck();
        Fly();
    }

    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

   

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
            }
        }
    } 
    private void Fly()
    {

             if (Input.GetKey(KeyCode.Space))
            {
                rb.AddForce(new Vector3(0, flyForce, 0), ForceMode.Impulse);
                
            }

    }
    
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
            GameManager.gameOver = true;
        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;
        }

        if (collision.gameObject.CompareTag("Agua"))
        {
            ControllerVida.AgarraAgua = true;
            Destroy(collision.gameObject);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
        }
    }
}
