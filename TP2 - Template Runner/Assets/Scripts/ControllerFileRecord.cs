using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class ControllerFileRecord : MonoBehaviour
{
    private string filePath;
    //public int maxRecord;
    // Start is called before the first frame update
   
     public void SaveRecord(int score, int maxRecord)
    {
        //LoadRecord
        // Crea un FileStream en modo de escritura
      if(score > maxRecord)
      {
        filePath = Application.persistentDataPath + "/record.bin";
        FileStream fs = new FileStream(filePath, FileMode.Create);
        BinaryWriter writer = new BinaryWriter(fs);

        // Escribe el record en el archivo binario
        writer.Write(score);

        // Cierra el BinaryWriter y el FileStream
        writer.Close();
        fs.Close();

      }  
    }
        public int LoadRecord()
    {
        filePath = Application.persistentDataPath + "/record.bin";
        int record = 0;

        // Si el archivo existe, leer el record
        if (File.Exists(filePath))
        {
            // Crea un FileStream en modo de lectura
            FileStream fs = new FileStream(filePath, FileMode.Open);
            BinaryReader reader = new BinaryReader(fs);

            // Lee el record del archivo binario
            record = reader.ReadInt32();

            // Cierra el BinaryReader y el FileStream
            reader.Close();
            fs.Close();
        }

        return record;
    }
}
